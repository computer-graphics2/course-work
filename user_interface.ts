import {ISIF, ISIFs} from "./isifs";

export function generateButtons(isifs: ISIFs, container: HTMLElement, callBack) {
    for(let key in isifs) {
        const button = document.createElement('button');
        button.classList.add('btn');
        button.classList.add('btn-primary');
        button.classList.add('btn-sif');
        button.innerText = key;
        button.addEventListener('click', (e) => {
            callBack(e, key);
        });
        container.appendChild(button);
    }

}

export function generateControls(container: HTMLFormElement, sif: ISIF, coeffNotation: string[]) {
    container.innerHTML = '';

    const inputsRows = sif.map((coeffs) => {
        const inputsRow = createInputsRow('inputs-row');
        coeffs.forEach((coeff, coeffIndex) => {
            const input = createInput({val: coeff, descr: coeffNotation[coeffIndex]});
            inputsRow.appendChild(input);
        });
        return inputsRow;
    });
    inputsRows.forEach((row) => {
        container.appendChild(row);
    });
}

export function createInput(params: {val: number, descr: string}): HTMLElement {
    const inputContainer = document.createElement('div');
    inputContainer.classList.add('input-container');
    const inputDescr = createTextSpan('inputs-container__descr', params.descr);
    const input = document.createElement('input');
    input.classList.add('input-container__input');
    input.classList.add('form-control');
    input.setAttribute('value', String(params.val));
    inputContainer.appendChild(inputDescr);
    inputContainer.appendChild(input);
    return inputContainer;
}

export function createInputsRow(className: string): HTMLElement {
    const inputsRow: HTMLElement = document.createElement('div');
    inputsRow.classList.add(className);
    return inputsRow;
}

export function createTextSpan(className: string, value: string): HTMLElement {
    const span = document.createElement('span');
    span.innerText = value;
    span.classList.add(className);
    return span;
}