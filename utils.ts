export function randomInt(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


export function randomFloat(min: number, max: number): number {
    return Math.random() * (max - min) + min;
}

export function generateColor(): [number, number, number, number] {
    return [
        randomInt(0, 255),
        randomInt(0, 255),
        randomInt(0, 255),
        randomInt(0, 255)
    ]
}