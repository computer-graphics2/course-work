// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"matrix.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMirrorImageMatrix = exports.getStretchingMatrix = exports.getTransfereMatrix = exports.projectionMatrixes = exports.getRotateMatrix = exports.mult_matrix_vector = exports.createMatrix = void 0;

function createMatrix(order) {
  var m = [];

  for (var i = 0; i < order; i++) {
    var str = new Array(order);
    str = str.map(function (num) {
      return 0;
    });
    m.push(str);
  }

  return m;
}

exports.createMatrix = createMatrix;

function mult_matrix_vector(m, v) {
  var result = new Array(v.length);

  for (var i = 0; i < m.length; i++) {
    result[i] = 0;

    for (var j = 0; j < v.length; j++) {
      result[i] += v[j] * m[j][i];
    }
  }

  return result;
}

exports.mult_matrix_vector = mult_matrix_vector;

function MatrixVectorMultiplication(A, X) {
  var A_l = A.length;

  for (var Y = new Array(A_l), j = 0; j < A_l; j++) {
    Y[j] = 0;

    for (var k = 0; k < A_l; k++) {
      Y[j] += A[j][k] * X[j];
    }
  }

  return Y;
}

function getRotateXMatrix(angle) {
  return [[1, 0, 0, 0], [0, Math.cos(angle), -Math.sin(angle), 0], [0, Math.sin(angle), Math.cos(angle), 0], [0, 0, 0, 1]];
}

function getRotateYMatrix(angle) {
  return [[Math.cos(angle), 0, Math.sin(angle), 0], [0, 1, 0, 0], [-Math.sin(angle), 0, Math.cos(angle), 0], [0, 0, 0, 1]];
}

function getRotateZMatrix(angle) {
  return [[Math.cos(angle), -Math.sin(angle), 0, 0], [Math.sin(angle), Math.cos(angle), 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]];
}

function translateDegToRad(deg) {
  return Math.PI / 180 * deg;
}

function getRotateMatrix(axis, angle) {
  var radDeg = translateDegToRad(angle);

  switch (axis) {
    case 'x':
      return getRotateXMatrix(radDeg);

    case 'y':
      return getRotateYMatrix(radDeg);

    case 'z':
      return getRotateZMatrix(radDeg);
  }
}

exports.getRotateMatrix = getRotateMatrix;

function getTransfereMatrix() {
  var x = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var y = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var z = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
  return [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [x, y, z, 1]];
}

exports.getTransfereMatrix = getTransfereMatrix;

function getStretchingMatrix() {
  var a = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
  var b = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  var c = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
  return [[a, 0, 0, 0], [0, b, 0, 0], [0, 0, c, 0], [0, 0, 0, 1]];
}

exports.getStretchingMatrix = getStretchingMatrix;

function getMirrorImageMatrixXY() {
  return [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]];
}

function getMirrorImageMatrixYZ() {
  return [[-1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]];
}

function getMirrorImageMatrixXZ() {
  return [[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]];
}

function getMirrorImageMatrix(axe) {
  switch (axe) {
    case 'xy':
      return getMirrorImageMatrixXY();

    case 'yz':
      return getMirrorImageMatrixYZ();

    case 'xz':
      return getMirrorImageMatrixXZ();
  }
}

exports.getMirrorImageMatrix = getMirrorImageMatrix;

function getSkewMatrix() {
  return [[1, 1, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]];
}

function getOrthographicProjectionMatrixX(p) {
  return [[0, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [p, 0, 0, 1]];
}

function getOrthographicProjectionMatrixY(p) {
  return [[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 1, 0], [0, p, 0, 1]];
}

function getOrthographicProjectionMatrixZ(p) {
  return [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 0], [0, 0, p, 1]];
}

function getAxonometricProjectioтMatrixZ(angleY, angleX) {
  angleX = translateDegToRad(angleX);
  angleY = translateDegToRad(angleY);
  return [[Math.cos(angleY), Math.sin(angleX) * Math.sin(angleY), 0, 0], [0, Math.cos(angleY), 0, 0], [Math.sin(angleY), -Math.sin(angleY) * Math.cos(angleY), 0, 0], [0, 0, 0, 1]];
}

function getOrthographicProjectionMatrix() {
  var axis = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'z';
  var p = arguments.length > 1 ? arguments[1] : undefined;

  switch (axis) {
    case 'x':
      return getOrthographicProjectionMatrixX(p);

    case 'y':
      return getOrthographicProjectionMatrixY(p);

    default:
      return getOrthographicProjectionMatrixZ(p);
  }
}

function getCabinetProjectionMatrix() {
  return getObliqueProjectionMatrix(0.5 * Math.cos(Math.PI / 4), 0.5 * Math.cos(Math.PI / 4));
}

function getObliqueProjectionMatrix(a, b) {
  return [[1, 0, 0, 0], [0, 1, 0, 0], [a, b, 0, 0], [0, 0, 0, 1]];
}

function getFreeProjectionMatrix() {
  return getObliqueProjectionMatrix(Math.cos(Math.PI / 4), Math.cos(Math.PI / 4));
}

function getCentralProjectionMatrix(a, b, c) {
  return [[1, 0, 0, -1 / a], [0, 1, 0, -1 / b], [0, 0, 1, -1 / c], [0, 0, 0, 1]];
}

function getSinglePointProjectionMatrix(r) {
  return [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1 / r], [0, 0, 0, 1]];
}

var projectionMatrixes = {
  orthography: getOrthographicProjectionMatrix('z', 0),
  axonometric: getAxonometricProjectioтMatrixZ(10, 10),
  cabinet: getCabinetProjectionMatrix(),
  free: getFreeProjectionMatrix(),
  central: getCentralProjectionMatrix(1000, 1000, 1000),
  singlePoint: getSinglePointProjectionMatrix(800)
};
exports.projectionMatrixes = projectionMatrixes;
},{}],"lr2.ts":[function(require,module,exports) {
"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

Object.defineProperty(exports, "__esModule", {
  value: true
});

var matrix_1 = require("./matrix");

function getCubeVertexes(s, canvasWidth, canvasHeight) {
  return [[s + canvasWidth / 2, -s + canvasHeight / 2, -s], [s + canvasWidth / 2, -s + canvasHeight / 2, s], [-s + canvasWidth / 2, -s + canvasHeight / 2, s], [-s + canvasWidth / 2, -s + canvasHeight / 2, -s], [s + canvasWidth / 2, s + canvasHeight / 2, -s], [s + canvasWidth / 2, s + canvasHeight / 2, s], [-s + canvasWidth / 2, s + canvasHeight / 2, s], [-s + canvasWidth / 2, s + canvasHeight / 2, -s]];
}

function createPolygons2(vxs) {
  return [[vxs[1], vxs[2], vxs[6], vxs[5]], [vxs[0], vxs[3], vxs[7], vxs[4]], [vxs[3], vxs[2], vxs[6], vxs[7]], [vxs[0], vxs[1], vxs[5], vxs[4]], [vxs[5], vxs[6], vxs[7], vxs[4]] // top
  , [vxs[0], vxs[1], vxs[2], vxs[3]] // bot
  ];
}

function createPolygons(s) {
  return [createFrontPolygon(s), createBackPolygon(s), createLeftPolygon(s), createRightPolygon(s), createTopPolygon(s), createBottomPolygon(s)];
}

function createFrontPolygon(s) {
  var p1 = [0, 0, -s];
  var p2 = [0, -s, -s];
  var p3 = [s, -s, -s];
  var p4 = [s, 0, -s];
  return [p1, p2, p3, p4];
}

function createBackPolygon(s) {
  var p1 = [0, 0, 0];
  var p2 = [0, -s, 0];
  var p3 = [s, -s, 0];
  var p4 = [s, 0, 0];
  return [p1, p2, p3, p4];
}

function createTopPolygon(s) {
  var p1 = [0, -s, -s];
  var p2 = [0, -s, 0];
  var p3 = [s, -s, 0];
  var p4 = [s, -s, s];
  return [p1, p2, p3, p4];
}

function createBottomPolygon(s) {
  var p1 = [0, 0, -s];
  var p2 = [0, 0, 0];
  var p3 = [s, 0, 0];
  var p4 = [s, 0, -s];
  return [p1, p2, p3, p4];
}

function createLeftPolygon(s) {
  var p1 = [0, 0, -s];
  var p2 = [0, -s, -s];
  var p3 = [0, -s, 0];
  var p4 = [0, 0, 0];
  return [p1, p2, p3, p4];
}

function createRightPolygon(s) {
  var p1 = [s, 0, -s];
  var p2 = [s, -s, -s];
  var p3 = [s, -s, 0];
  var p4 = [s, 0, 0];
  return [p1, p2, p3, p4];
}

function convertPoint(p, transformMatrixes) {
  var result = [].concat(_toConsumableArray(p), [1]);

  for (var i = 0; i < transformMatrixes.length; i++) {
    var transformMatrix = transformMatrixes[i];
    result = matrix_1.mult_matrix_vector(transformMatrix, result);
    result = normalizeVector(result);
  }

  var newPoint = [result[0], result[1], result[2]];
  return newPoint;
}

function convertFigure(figure, transformMatrixes) {
  return figure.map(function (polygon) {
    return polygon.map(function (p) {
      return convertPoint([p[0], p[1], p[2]], transformMatrixes);
    });
  });
}

function normalizeVector(vector) {
  return vector.map(function (elem) {
    return elem / vector[vector.length - 1];
  });
}

function clip(p) {
  var point = _toConsumableArray(p);

  point[0] = Math.min(Math.max(0, Math.round(p[0])));
  point[1] = Math.min(Math.max(0, Math.round(p[1])));
  return point;
}

function createMatrix(width, height) {
  var initValue = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
  var matrix = [];

  for (var i = 0; i < height; i++) {
    var row = [];

    for (var j = 0; j < width; j++) {
      row.push(initValue);
    }

    matrix.push(row);
  }

  return matrix;
}

function project(p, projectionMatrix, canvasWidth, canvasHeight) {
  var offsetX = 0; //canvasWidth / 2;

  var offsetY = 0; //canvasHeight / 2;
  //p[0] += offsetX;
  //p[1] += offsetY;

  var point2D = matrix_1.mult_matrix_vector(projectionMatrix, [].concat(_toConsumableArray(p), [1])).slice(0, 3);
  point2D[0] += offsetX;
  point2D[1] += offsetY;
  var clipPoint = clip([point2D[0], point2D[1]]);
  return clipPoint;
}

function calcPlaneEquation(points) {
  var a = 0;
  var b = 0;
  var c = 0;
  var d = 0;
  points.forEach(function (p, i) {
    var j = i === points.length - 1 ? 0 : i + 1;
    var p2 = points[j];
    a += (p[1] - p2[1]) * (p[2] + p2[2]);
    b += (p[2] - p2[2]) * (p[0] + p2[0]);
    c += (p[0] - p2[0]) * (p[1] + p2[1]);
  });
  d = -(a * points[points.length - 1][0] + b * points[points.length - 1][1] + c * points[points.length - 1][2]);
  return [a, b, c, d];
}

function getPlaneEquation(points) {
  var v1 = [points[1][0] - points[0][0], points[1][1] - points[0][1], points[1][2] - points[0][2]];
  var v2 = [points[2][0] - points[0][0], points[2][1] - points[0][1], points[2][2] - points[0][2]];
  var result = [v1[1] * v2[2] - v1[2] * v2[1], -(v1[0] * v2[2] - v1[2] * v2[0]), v1[0] * v2[1] - v1[1] * v2[0]];
  var d = -(result[0] * points[0][0] + result[1] * points[0][1] + result[2] * points[0][2]);
  return [result[0], result[1], result[2], d];
}

function calcPixelZ(p, planeEquation) {
  var c = planeEquation[2] === 0 ? 0.00000001 : planeEquation[2];
  return -(p[0] * planeEquation[0] + p[1] * planeEquation[1] + planeEquation[3]) / c;
}

function getBorders(points) {
  var leftX = Infinity;
  var rightX = -Infinity;
  var topY = Infinity;
  var botY = -Infinity;
  points.forEach(function (p) {
    leftX = p[0] < leftX ? p[0] : leftX;
    rightX = p[0] > rightX ? p[0] : rightX;
    topY = p[1] < topY ? p[1] : topY;
    botY = p[1] > botY ? p[1] : botY;
  });
  return {
    left: leftX,
    right: rightX,
    top: topY,
    bottom: botY
  };
}
/*
* TODO
*  Ищем границы для 3D
*
* */


function fillZBuffer(params) {
  var polygons = params.polygons,
      screenWidth = params.screenWidth,
      screenHeight = params.screenHeight,
      projectionMatrix = params.projectionMatrix,
      baseColorNum = params.baseColorNum,
      ctx = params.ctx,
      imageData = params.imageData;
  var zBuffer = createMatrix(SCREEN_WIDTH, SCREEN_HEIGHT, -Infinity);
  var colorBuffer = createMatrix(SCREEN_WIDTH, SCREEN_HEIGHT, baseColorNum);
  polygons.forEach(function (polygon, polygonIndex) {
    var plane = calcPlaneEquation(polygon);
    var polygon2D = polygon.map(function (p) {
      return project([p[0], p[1], p[2]], projectionMatrix, screenWidth, screenHeight);
    });
    var polygonBorders = getBorders(polygon2D);

    for (var y = polygonBorders.top; y <= polygonBorders.bottom; y++) {
      for (var x = polygonBorders.left; x <= polygonBorders.right; x++) {
        if (!isBelongPolygon(x, y, polygon2D)) continue;
        var z = calcPixelZ([x, y], plane);
        if (z < zBuffer[y][x]) continue;
        zBuffer[y][x] = z;
        colorBuffer[y][x] = polygonIndex;
      }
    }
  });
  return {
    zBuffer: zBuffer,
    colorBuffer: colorBuffer
  };
}

function draw(ctx, imageData, canvasWidth, canvasHeight, colorBuffer) {
  var data = imageData.data;

  for (var i = 0; i < data.length; i += 4) {
    var x = i / 4 % canvasWidth;
    var y = Math.floor(i / 4 / canvasHeight);
    data[i + 3] = 255;
    var color = colorBuffer[y][x];
    data[i] = colors[color][0];
    data[i + 1] = colors[color][1];
    data[i + 2] = colors[color][2];
  }

  ctx.putImageData(imageData, 0, 0);
}

var SCREEN_WIDTH = 200;
var SCREEN_HEIGHT = 200;
var CUBE_SIZE = 10;
var BASE_COLOR_NUM = 26;
var ANGLE = 30;
var ANGLE2 = 60;
var colors = [[255, 0, 0], [0, 0, 255], [0, 255, 0], [0, 255, 255], [255, 255, 0], [255, 0, 255], [255, 0, 0], [0, 0, 255], [0, 255, 0], [0, 255, 255], [255, 255, 0], [255, 0, 255], [255, 0, 0], [0, 0, 255], [0, 255, 0], [0, 255, 255], [255, 255, 0], [255, 0, 255], [255, 255, 0], [255, 0, 255], [255, 0, 0], [0, 0, 255], [0, 255, 0], [0, 255, 255], [255, 255, 0], [255, 0, 255], [128, 128, 128]];

var cubePolygons = _toConsumableArray(createPolygons2(getCubeVertexes(CUBE_SIZE, SCREEN_WIDTH, SCREEN_HEIGHT)));

var cube2Polygons = _toConsumableArray(createPolygons2(getCubeVertexes(CUBE_SIZE + 10, SCREEN_WIDTH + 20, SCREEN_HEIGHT - 40)));

function isBelongPolygon(x, y, polygon) {
  var lines = [];
  var c = false;

  for (var i = 0; i < polygon.length; i++) {
    var j = i === polygon.length - 1 ? 0 : i + 1;
    lines.push([polygon[i], polygon[j]]);
  }

  lines.forEach(function (line) {
    var v1 = line[0];
    var v2 = line[1];
    var v1x = v1[0];
    var v1y = v1[1];
    var v2x = v2[0];
    var v2y = v2[1];

    if ((v2y <= y && y < v1y || v1y <= y && y < v2y) && x > (v1x - v2x) * (y - v2y) / (v1y - v2y) + v2x) {
      c = !c;
    }
  });
  return c;
}

var canvas = document.getElementById('canvas');
canvas.setAttribute('width', String(SCREEN_WIDTH));
canvas.setAttribute('height', String(SCREEN_HEIGHT));
var ctx = canvas.getContext('2d');
var imageData = ctx.getImageData(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
var transformCube1Polygons = convertFigure(cubePolygons, [matrix_1.getRotateMatrix('x', ANGLE), matrix_1.getRotateMatrix('y', ANGLE), matrix_1.getTransfereMatrix(0, 0, 0)]);
var transformCube2Polygons = convertFigure(cube2Polygons, [matrix_1.getRotateMatrix('x', -ANGLE2), matrix_1.getRotateMatrix('y', -ANGLE2), matrix_1.getTransfereMatrix(0, 30, 55)]);

var _fillZBuffer = fillZBuffer({
  polygons: [].concat(_toConsumableArray(transformCube1Polygons), _toConsumableArray(transformCube2Polygons)),
  screenWidth: SCREEN_WIDTH,
  screenHeight: SCREEN_HEIGHT,
  projectionMatrix: matrix_1.projectionMatrixes.orthography,
  baseColorNum: BASE_COLOR_NUM,
  ctx: ctx,
  imageData: imageData
}),
    colorBuffer = _fillZBuffer.colorBuffer;

draw(ctx, imageData, SCREEN_WIDTH, SCREEN_HEIGHT, colorBuffer);
var angle1 = 0;
var angle2 = 0; // setInterval(() => {
//     angle1++;
//     angle2--;
//
//     const transformCube1Polygons = convertFigure(cubePolygons,
//         [getRotateMatrix('x', angle1),getRotateMatrix('y', angle1), getTransfereMatrix(0, 0, 80)]);
//     const transformCube2Polygons = convertFigure(cube2Polygons, [getRotateMatrix('x', angle2),getRotateMatrix('y', angle2), getTransfereMatrix(0, 30, 0)])
//     const {colorBuffer} = fillZBuffer({
//         polygons: [...transformCube1Polygons, ...transformCube2Polygons],
//         screenWidth: SCREEN_WIDTH,
//         screenHeight: SCREEN_HEIGHT,
//         projectionMatrix: projectionMatrixes.orthography,
//         baseColorNum: BASE_COLOR_NUM,
//         ctx,
//         imageData
//     });
//     draw(ctx, imageData, SCREEN_WIDTH, SCREEN_HEIGHT, colorBuffer);
// }, 10);
},{"./matrix":"matrix.ts"}],"C:/Users/vishn/AppData/Roaming/npm/node_modules/parcel/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "56429" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["C:/Users/vishn/AppData/Roaming/npm/node_modules/parcel/src/builtins/hmr-runtime.js","lr2.ts"], null)
//# sourceMappingURL=/lr2.302cba5d.js.map