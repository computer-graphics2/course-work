// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"matrix.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMirrorImageMatrix = exports.getStretchingMatrix = exports.getTransfereMatrix = exports.projectionMatrixes = exports.getRotateMatrix = exports.mult_matrix_vector = exports.createMatrix = void 0;

function createMatrix(order) {
  var m = [];

  for (var i = 0; i < order; i++) {
    var str = new Array(order);
    str = str.map(function (num) {
      return 0;
    });
    m.push(str);
  }

  return m;
}

exports.createMatrix = createMatrix;

function mult_matrix_vector(m, v) {
  var result = new Array(v.length);

  for (var i = 0; i < m.length; i++) {
    result[i] = 0;

    for (var j = 0; j < v.length; j++) {
      result[i] += v[j] * m[j][i];
    }
  }

  return result;
}

exports.mult_matrix_vector = mult_matrix_vector;

function MatrixVectorMultiplication(A, X) {
  var A_l = A.length;

  for (var Y = new Array(A_l), j = 0; j < A_l; j++) {
    Y[j] = 0;

    for (var k = 0; k < A_l; k++) {
      Y[j] += A[j][k] * X[j];
    }
  }

  return Y;
}

function getRotateXMatrix(angle) {
  return [[1, 0, 0, 0], [0, Math.cos(angle), -Math.sin(angle), 0], [0, Math.sin(angle), Math.cos(angle), 0], [0, 0, 0, 1]];
}

function getRotateYMatrix(angle) {
  return [[Math.cos(angle), 0, Math.sin(angle), 0], [0, 1, 0, 0], [-Math.sin(angle), 0, Math.cos(angle), 0], [0, 0, 0, 1]];
}

function getRotateZMatrix(angle) {
  return [[Math.cos(angle), -Math.sin(angle), 0, 0], [Math.sin(angle), Math.cos(angle), 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]];
}

function translateDegToRad(deg) {
  return Math.PI / 180 * deg;
}

function getRotateMatrix(axis, angle) {
  var radDeg = translateDegToRad(angle);

  switch (axis) {
    case 'x':
      return getRotateXMatrix(radDeg);

    case 'y':
      return getRotateYMatrix(radDeg);

    case 'z':
      return getRotateZMatrix(radDeg);
  }
}

exports.getRotateMatrix = getRotateMatrix;

function getTransfereMatrix() {
  var x = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var y = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var z = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
  return [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [x, y, z, 1]];
}

exports.getTransfereMatrix = getTransfereMatrix;

function getStretchingMatrix() {
  var a = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
  var b = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  var c = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
  return [[a, 0, 0, 0], [0, b, 0, 0], [0, 0, c, 0], [0, 0, 0, 1]];
}

exports.getStretchingMatrix = getStretchingMatrix;

function getMirrorImageMatrixXY() {
  return [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]];
}

function getMirrorImageMatrixYZ() {
  return [[-1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]];
}

function getMirrorImageMatrixXZ() {
  return [[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]];
}

function getMirrorImageMatrix(axe) {
  switch (axe) {
    case 'xy':
      return getMirrorImageMatrixXY();

    case 'yz':
      return getMirrorImageMatrixYZ();

    case 'xz':
      return getMirrorImageMatrixXZ();
  }
}

exports.getMirrorImageMatrix = getMirrorImageMatrix;

function getSkewMatrix() {
  return [[1, 1, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]];
}

function getOrthographicProjectionMatrixX(p) {
  return [[0, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [p, 0, 0, 1]];
}

function getOrthographicProjectionMatrixY(p) {
  return [[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 1, 0], [0, p, 0, 1]];
}

function getOrthographicProjectionMatrixZ(p) {
  return [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 0], [0, 0, p, 1]];
}

function getAxonometricProjectioтMatrixZ(angleY, angleX) {
  angleX = translateDegToRad(angleX);
  angleY = translateDegToRad(angleY);
  return [[Math.cos(angleY), Math.sin(angleX) * Math.sin(angleY), 0, 0], [0, Math.cos(angleY), 0, 0], [Math.sin(angleY), -Math.sin(angleY) * Math.cos(angleY), 0, 0], [0, 0, 0, 1]];
}

function getOrthographicProjectionMatrix() {
  var axis = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'z';
  var p = arguments.length > 1 ? arguments[1] : undefined;

  switch (axis) {
    case 'x':
      return getOrthographicProjectionMatrixX(p);

    case 'y':
      return getOrthographicProjectionMatrixY(p);

    default:
      return getOrthographicProjectionMatrixZ(p);
  }
}

function getCabinetProjectionMatrix() {
  return getObliqueProjectionMatrix(0.5 * Math.cos(Math.PI / 4), 0.5 * Math.cos(Math.PI / 4));
}

function getObliqueProjectionMatrix(a, b) {
  return [[1, 0, 0, 0], [0, 1, 0, 0], [a, b, 0, 0], [0, 0, 0, 1]];
}

function getFreeProjectionMatrix() {
  return getObliqueProjectionMatrix(Math.cos(Math.PI / 4), Math.cos(Math.PI / 4));
}

function getCentralProjectionMatrix(a, b, c) {
  return [[1, 0, 0, -1 / a], [0, 1, 0, -1 / b], [0, 0, 1, -1 / c], [0, 0, 0, 1]];
}

function getSinglePointProjectionMatrix(r) {
  return [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1 / r], [0, 0, 0, 1]];
}

var projectionMatrixes = {
  orthography: getOrthographicProjectionMatrix('z', 0),
  axonometric: getAxonometricProjectioтMatrixZ(10, 10),
  cabinet: getCabinetProjectionMatrix(),
  free: getFreeProjectionMatrix(),
  central: getCentralProjectionMatrix(1000, 1000, 1000),
  singlePoint: getSinglePointProjectionMatrix(800)
};
exports.projectionMatrixes = projectionMatrixes;
},{}],"index.ts":[function(require,module,exports) {
"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

Object.defineProperty(exports, "__esModule", {
  value: true
});

var matrix_1 = require("./matrix");

var colors = [[255, 0, 0], [0, 0, 255], [0, 255, 0], [0, 255, 255], [255, 255, 0], [255, 0, 255], [128, 128, 128]];
var MIN_DIST = -999999999;
var BASE_COLOR_NUM = 6;

var Polygon = /*#__PURE__*/function () {
  function Polygon(p) {
    var _this = this;

    _classCallCheck(this, Polygon);

    this.points = [];

    this.getPoints = function () {
      return _this.points;
    };

    this.points = p;
    this.matrixOperations = new MatrixOperations();
  }

  _createClass(Polygon, [{
    key: "rotate",
    value: function rotate(axe, angle) {
      var _this2 = this;

      this.points = this.points.map(function (p) {
        var newP = _this2.matrixOperations.multMatrixVector(matrix_1.getRotateMatrix(axe, angle), [].concat(_toConsumableArray(p), [1])).slice(0, 3);

        return newP;
      });
    }
  }]);

  return Polygon;
}();

var MatrixOperations = /*#__PURE__*/function () {
  function MatrixOperations() {
    _classCallCheck(this, MatrixOperations);
  }

  _createClass(MatrixOperations, [{
    key: "multMatrixVector",
    value: function multMatrixVector(m, v) {
      var result = new Array(v.length);

      for (var i = 0; i < m.length; i++) {
        result[i] = 0;

        for (var j = 0; j < v.length; j++) {
          result[i] += v[j] * m[j][i];
        }
      }

      return result;
    }
  }]);

  return MatrixOperations;
}();

var Projector = /*#__PURE__*/function () {
  function Projector(params) {
    _classCallCheck(this, Projector);

    this.projectionMatrix = params.projectionMatrix;
    this.matrixOperations = new MatrixOperations();
    this.zBuffer = this.createZBuffer(params.screenWidth, params.screenHeight);
    this.frameBuffer = this.createFrameBuffer(params.screenWidth, params.screenHeight);
    this.screenWidth = params.screenWidth;
    this.screenHeight = params.screenHeight;
  }

  _createClass(Projector, [{
    key: "getZBuffer",
    value: function getZBuffer() {
      return this.zBuffer;
    }
  }, {
    key: "getFrameBuffer",
    value: function getFrameBuffer() {
      return this.frameBuffer;
    }
  }, {
    key: "project",
    value: function project(cube) {
      var _this3 = this;

      var polygons = cube.getPolygons();

      for (var polygonIndex = 0; polygonIndex < polygons.length; polygonIndex++) {
        var polygon = polygons[polygonIndex]; //polygon.rotate('x',45);

        polygon.rotate('y', 60);
        var pts = polygon.getPoints();
        var planeEquation = getPlaneEquation(pts);
        var polygonPoints = polygon.getPoints().map(function (p) {
          var point2D = _this3.matrixOperations.multMatrixVector(_this3.projectionMatrix, [].concat(_toConsumableArray(p), [1]));

          var xCoords = Math.round(point2D[0] / point2D[3]);
          var yCoords = Math.round(point2D[1] / point2D[3]);
          return [xCoords + _this3.screenWidth / 2, yCoords + _this3.screenHeight / 2];
        });
        var borders = getBorders(polygonPoints);

        for (var y = borders.top; y <= borders.bottom; y++) {
          for (var x = borders.left; x <= borders.right; x++) {
            var z = calcZ([x, y], planeEquation);

            if (z > this.zBuffer[y][x]) {
              this.zBuffer[y][x] = z;
              this.frameBuffer[y][x] = polygonIndex;
            }
          }
        }
      }
    }
  }, {
    key: "createZBuffer",
    value: function createZBuffer(screenWidth, screenHeight) {
      return JSON.parse(JSON.stringify(new Array(screenHeight).fill(new Array(screenWidth).fill(MIN_DIST))));
    }
  }, {
    key: "createFrameBuffer",
    value: function createFrameBuffer(screenWidth, screenHeight) {
      return JSON.parse(JSON.stringify(new Array(screenHeight).fill(new Array(screenWidth).fill(BASE_COLOR_NUM))));
    }
  }]);

  return Projector;
}();

var Cube = /*#__PURE__*/function () {
  function Cube(size) {
    var _this4 = this;

    _classCallCheck(this, Cube);

    this.getSize = function () {
      return _this4.size;
    };

    this.getPolygons = function () {
      return _this4.polygons;
    };

    this.size = size;
    this.polygons = this.createPolygons();
  }

  _createClass(Cube, [{
    key: "createPolygons",
    value: function createPolygons() {
      return [this.createFrontPolygon(), this.createBackPolygon(), this.createLeftPolygon(), this.createRightPolygon(), this.createTopPolygon(), this.createBottomPolygon()];
    }
  }, {
    key: "createFrontPolygon",
    value: function createFrontPolygon() {
      var s = this.size;
      var p1 = [0, 0, s];
      var p2 = [0, s, s];
      var p3 = [s, s, s];
      var p4 = [s, 0, s];
      return new Polygon([p1, p2, p3, p4]);
    }
  }, {
    key: "createBackPolygon",
    value: function createBackPolygon() {
      var s = this.size;
      var p1 = [0, 0, 0];
      var p2 = [0, s, 0];
      var p3 = [s, s, 0];
      var p4 = [s, 0, 0];
      return new Polygon([p1, p2, p3, p4]);
    }
  }, {
    key: "createTopPolygon",
    value: function createTopPolygon() {
      var s = this.size;
      var p1 = [0, s, s];
      var p2 = [0, s, 0];
      var p3 = [s, s, 0];
      var p4 = [s, s, s];
      return new Polygon([p1, p2, p3, p4]);
    }
  }, {
    key: "createBottomPolygon",
    value: function createBottomPolygon() {
      var s = this.size;
      var p1 = [0, 0, s];
      var p2 = [0, 0, 0];
      var p3 = [s, 0, 0];
      var p4 = [s, 0, s];
      return new Polygon([p1, p2, p3, p4]);
    }
  }, {
    key: "createLeftPolygon",
    value: function createLeftPolygon() {
      var s = this.size;
      var p1 = [0, 0, s];
      var p2 = [0, s, s];
      var p3 = [0, s, 0];
      var p4 = [0, 0, 0];
      return new Polygon([p1, p2, p3, p4]);
    }
  }, {
    key: "createRightPolygon",
    value: function createRightPolygon() {
      var s = this.size;
      var p1 = [s, 0, s];
      var p2 = [s, s, s];
      var p3 = [s, s, 0];
      var p4 = [s, 0, 0];
      return new Polygon([p1, p2, p3, p4]);
    }
  }]);

  return Cube;
}();

function getPlaneEquation(points) {
  var a = 0;
  var b = 0;
  var c = 0;
  var d = 0;
  points.forEach(function (p, i) {
    var j = i === points.length - 1 ? 0 : i + 1;
    var p2 = points[j];
    a += (p[1] - p2[1]) * (p[2] + p2[2]);
    b += (p[2] - p2[2]) * (p[0] + p2[0]);
    c += (p[0] - p2[0]) * (p[1] + p2[1]);
  });
  d = -(a * points[points.length - 1][0] + b * points[points.length - 1][1] + c * points[points.length - 1][2]);
  return [a, b, c, d];
}

function calcZ(p, planeEquation) {
  var c = planeEquation[2];
  return -(p[0] * planeEquation[0] + p[1] * planeEquation[1] + planeEquation[3]) / c;
}

function getBorders(points) {
  var leftX = Infinity;
  var rightX = -Infinity;
  var topY = Infinity;
  var botY = -Infinity;
  points.forEach(function (p) {
    leftX = p[0] < leftX ? p[0] : leftX;
    rightX = p[0] > rightX ? p[0] : rightX;
    topY = p[1] < topY ? p[1] : topY;
    botY = p[1] > botY ? p[1] : botY;
  });
  return {
    left: leftX,
    right: rightX,
    top: topY,
    bottom: botY
  };
}

var CANVAS_WIDTH = 30;
var CANVAS_HEIGHT = 30;

function draw(ctx, imageData) {
  var data = imageData.data;

  for (var i = 0; i < data.length; i += 4) {
    var x = i / 4 % CANVAS_WIDTH;
    var y = Math.floor(i / 4 / CANVAS_WIDTH);
    data[i + 3] = 255;
    var color = projector.getFrameBuffer()[y][x];
    data[i] = colors[color][0];
    data[i + 1] = colors[color][1];
    data[i + 2] = colors[color][2];
  }

  ctx.putImageData(imageData, 0, 0);
}

var canvas = document.getElementById('canvas');
canvas.setAttribute('width', String(CANVAS_WIDTH));
canvas.setAttribute('height', String(CANVAS_HEIGHT));
var ctx = canvas.getContext('2d');
var imageData = ctx.getImageData(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
var projector = new Projector({
  projectionMatrix: matrix_1.projectionMatrixes.singlePoint,
  screenWidth: CANVAS_WIDTH,
  screenHeight: CANVAS_HEIGHT
});
var cube = new Cube(10);
projector.project(cube);
draw(ctx, imageData);
},{"./matrix":"matrix.ts"}],"C:/Users/vishn/AppData/Roaming/npm/node_modules/parcel/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "50140" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["C:/Users/vishn/AppData/Roaming/npm/node_modules/parcel/src/builtins/hmr-runtime.js","index.ts"], null)
//# sourceMappingURL=/lr2.77de5100.js.map