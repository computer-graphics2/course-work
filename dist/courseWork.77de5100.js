// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"isifs.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SIFs = void 0;
exports.SIFs = {
  'Папоротник': [[0, 0, 0, 0.16, 0, 0, 0.01], [0.85, 0.04, -0.04, 0.85, 0, 1.6, 0.85], [0.2, -0.26, 0.23, 0.22, 0, 1.6, 0.07], [-0.15, 0.28, 0.26, 0.24, 0, 0.44, 0.07]],
  'Решетка': [[0.3, -0.3, 0.3, 0.3, 1, 1, 0.25], [0.3, -0.3, 0.3, 0.3, 1, -1, 0.25], [0.3, -0.3, 0.3, 0.3, -1, 1, 0.25], [0.3, -0.3, 0.3, 0.3, -1, -1, 0.25]],
  'dragon1': [[0.824074, 0.281482, -0.212346, 0.864198, -1.882290, -0.110607, 0.787473], [0.088272, 0.520988, -0.463889, -0.377778, 0.785360, 8.095795, 0.212527]],
  'dragon': [[-0.5, -0.5, 0.5, -0.5, 490, 120, 0.5], [0.5, -0.5, 0.5, 0.5, 340, -110, 0.5]],
  'potato': [[0.47, 0.17, -0.17, 0.47, 0.00, 0.00, 0.45], [-0.11, 0.77, -0.77, -0.11, 1.11, 0.77, 0.55]],
  'banana': [[0.358591, 0.329160, -0.334007, 0.580720, -5.889050, 1.924278, 0.361396], [0.796218, 0.136942, 0.075187, 0.719074, 1.086998, 0.408452, 0.638604]],
  'carrot': [[0.336735, 0.000000, 0.000000, 0.330097, -0.161633, 0.610194, 0.143], [0.326531, 0.000000, 0.000000, 0.330097, 0.143265, 0.610194, 0.143], [0.091837, -0.281553, 0.336735, 0.106796, -0.247188, 0.551960, 0.143], [0.275510, -0.174757, 0.214286, 0.271845, -0.061759, 0.370832, 0.143], [0.265306, 0.184466, -0.204082, 0.262136, 0.041585, 0.372231, 0.143], [0.102041, 0.300971, -0.336735, 0.097087, 0.222962, 0.545807, 0.143], [0.326531, 0.000000, 0.010204, 0.339806, -0.006735, 0.104714, 0.143]],
  'Елочки': [[0.788060, -0.552239, -0.352239, -0.791045, 2.744518, 6.550929, 0.889610], [-0.149254, -0.402985, -0.191045, 0.164179, -4.915581, 5.265871, 0.110390]],
  'cucumber': [[-0.62, 0, 0, -0.62, 0, 0, 0.5], [0, -0.6, -0.6, 0, 1, 0.6, 0.5]],
  'tomato': [[-0.54, -0.58, 0.58, -0.54, 0, 0, 0.61], [-0.41, 0.33, -0.33, -0.41, 1.41, 0.33, 0.39]],
  'rorschach': [[-0.4, 0.51, -0.51, -0.4, 0.0, 0.00, 0.5], [-0.4, -0.51, 0.51, -0.4, 1.4, -0.51, 0.5]]
};
},{}],"utils.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateColor = exports.randomFloat = exports.randomInt = void 0;

function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

exports.randomInt = randomInt;

function randomFloat(min, max) {
  return Math.random() * (max - min) + min;
}

exports.randomFloat = randomFloat;

function generateColor() {
  return [randomInt(0, 255), randomInt(0, 255), randomInt(0, 255), randomInt(0, 255)];
}

exports.generateColor = generateColor;
},{}],"user_interface.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createTextSpan = exports.createInputsRow = exports.createInput = exports.generateControls = exports.generateButtons = void 0;

function generateButtons(isifs, container, callBack) {
  var _loop_1 = function _loop_1(key) {
    var button = document.createElement('button');
    button.classList.add('btn');
    button.classList.add('btn-primary');
    button.classList.add('btn-sif');
    button.innerText = key;
    button.addEventListener('click', function (e) {
      callBack(e, key);
    });
    container.appendChild(button);
  };

  for (var key in isifs) {
    _loop_1(key);
  }
}

exports.generateButtons = generateButtons;

function generateControls(container, sif, coeffNotation) {
  container.innerHTML = '';
  var inputsRows = sif.map(function (coeffs) {
    var inputsRow = createInputsRow('inputs-row');
    coeffs.forEach(function (coeff, coeffIndex) {
      var input = createInput({
        val: coeff,
        descr: coeffNotation[coeffIndex]
      });
      inputsRow.appendChild(input);
    });
    return inputsRow;
  });
  inputsRows.forEach(function (row) {
    container.appendChild(row);
  });
}

exports.generateControls = generateControls;

function createInput(params) {
  var inputContainer = document.createElement('div');
  inputContainer.classList.add('input-container');
  var inputDescr = createTextSpan('inputs-container__descr', params.descr);
  var input = document.createElement('input');
  input.classList.add('input-container__input');
  input.classList.add('form-control');
  input.setAttribute('value', String(params.val));
  inputContainer.appendChild(inputDescr);
  inputContainer.appendChild(input);
  return inputContainer;
}

exports.createInput = createInput;

function createInputsRow(className) {
  var inputsRow = document.createElement('div');
  inputsRow.classList.add(className);
  return inputsRow;
}

exports.createInputsRow = createInputsRow;

function createTextSpan(className, value) {
  var span = document.createElement('span');
  span.innerText = value;
  span.classList.add(className);
  return span;
}

exports.createTextSpan = createTextSpan;
},{}],"index.ts":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var isifs_1 = require("./isifs");

var utils_1 = require("./utils");

var user_interface_1 = require("./user_interface");

var canvas = document.getElementById("canvas");
var context = canvas.getContext('2d');
var CANVAS_WIDTH = 700;
var CANVAS_HEIGHT = 700;
canvas.setAttribute('width', String(CANVAS_WIDTH));
canvas.setAttribute('height', String(CANVAS_HEIGHT));

function getIF(sif) {
  var scale = 10000;
  var weight = Math.random() * scale;
  var IFLength = sif[0].length;
  var variationIndex = 0;
  var total = sif[variationIndex][IFLength - 1] * scale;

  while (weight > total) {
    variationIndex++;
    total += sif[variationIndex][IFLength - 1] * scale;
  }

  return sif[variationIndex];
}

function applyIF(point, IF) {
  var x = point[0];
  var y = point[1];
  var A = IF[0];
  var B = IF[1];
  var C = IF[2];
  var D = IF[3];
  var E = IF[4];
  var F = IF[5];
  var newX = A * x + B * y + E;
  var newY = C * x + D * y + F;
  return [newX, newY];
}

function main() {
  var coeffNotation = ['A', 'B', 'C', 'D', 'E', 'F', 'Probability'];
  var controlsContainer = document.getElementById('controls');
  var buttonsContainer = document.getElementById('sifs-buttons');
  var clearCanvasBtn = document.getElementById('clearCanvas');
  clearCanvasBtn.addEventListener('click', function () {
    context.clearRect(0, 0, canvas.width, canvas.height);
  });

  var btnClickHandler = function btnClickHandler(controlsContainer, coeffNotation) {
    return function (e, key) {
      var activeBtn = e.target.parentNode.querySelector('.btn.active');

      if (activeBtn) {
        activeBtn.classList.remove('active');
      }

      e.target.classList.add('active');
      user_interface_1.generateControls(controlsContainer, isifs_1.SIFs[key], coeffNotation);
    };
  };

  user_interface_1.generateButtons(isifs_1.SIFs, buttonsContainer, btnClickHandler(controlsContainer, coeffNotation)); // Adding listener to generate button

  var generateBtn = document.getElementById('generateButton');
  generateBtn.addEventListener('click', function () {
    var scaleInput = document.getElementById('scale-input');
    var translateXInput = document.getElementById('translate-x');
    var translateYInput = document.getElementById('translate-y');
    var iterationCountInput = document.getElementById('iteration-count');
    var dotCountInput = document.getElementById('dot-count');
    var scale = parseInt(scaleInput.value);
    var translateX = parseInt(translateXInput.value);
    var translateY = parseInt(translateYInput.value);
    var canvasWidth = canvas.clientWidth;
    var canvasHeight = canvas.clientHeight;
    var iterationCount = parseInt(iterationCountInput.value);
    var dotCount = parseInt(dotCountInput.value);
    asyncDrawFractal(canvas, getSIF(controlsContainer), {
      scale: scale,
      iterationCount: iterationCount,
      dotCount: dotCount,
      translateX: translateX,
      translateY: translateY,
      canvasWidth: canvasWidth,
      canvasHeight: canvasHeight,
      baseColor: [255, 0, 0, 255]
    }); //draw(canvas, getSIF(controlsContainer));
  });
}

main();

function getSIF(controlsContainer) {
  var inputs = controlsContainer.querySelectorAll('.input-container__input');
  var table = [];
  var row = [];

  for (var i = 1; i <= inputs.length; i++) {
    row.push(parseFloat(inputs[i - 1].value));

    if (i % 7 === 0) {
      table.push(row);
      row = [];
    }
  }

  return table;
}

function asyncDrawFractal(canvas, sif, params) {
  var _a, _b;

  if (!sif || sif.length === 0) {
    alert('Ошибка! Введите коэффициенты!');
    return;
  }

  var canvasWidth = params.canvasWidth,
      canvasHeight = params.canvasHeight,
      scale = params.scale,
      dotCount = params.dotCount,
      iterationCount = params.iterationCount,
      translateY = params.translateY,
      translateX = params.translateX,
      baseColor = params.baseColor;
  var histScale = 3;
  var gamma = 4;
  var histogram = createHistogram(canvasWidth, canvasHeight, histScale);
  console.log('Начали выполнение');

  for (var i = 0; i < dotCount; i++) {
    var _c = generatePoint(),
        x = _c[0],
        y = _c[1];

    _a = calcNewPoint([x, y], sif, iterationCount), x = _a[0], y = _a[1];
    _b = preparePoint([x, y], scale, canvasWidth, canvasHeight, translateX, translateY), x = _b[0], y = _b[1];

    if (x > 0 && x < canvas.width && y > 0 && y < canvas.height) {
      var randColor = utils_1.generateColor();
      var color = randColor.map(function (channelNumber, index) {
        return (channelNumber + baseColor[index]) / 2;
      });
      histogram = setHistogramColor(histogram, x, y, [color[0], color[1], color[2], color[3]]);
      asyncDrawPixel(context, [x, y], [color[0], color[1], color[2], color[3]]);
    }
  }

  console.log("Закончили!");
  var averageColorMatrix = getAverageColorMatrix(histogram, histScale);
  var averageFreqMatrix = getAverageFreqMatrix(histogram, histScale);
  var maxFrequency = getMaxFrequency(averageFreqMatrix);
  var alphaMatrix = getAlphaMatrix(averageFreqMatrix, maxFrequency);
  var finalPixelColorMatrix = getPixelColorMatrix(averageColorMatrix, averageFreqMatrix, alphaMatrix, gamma);
  console.log(alphaMatrix); // drawPixelColorMatrix(finalPixelColorMatrix, canvas, canvasWidth, canvasHeight);
}

function calcNewPoint(point, sif, iterationCount) {
  var _a;

  var x = point[0];
  var y = point[1];

  for (var i = 0; i < iterationCount; i++) {
    var IF = getIF(sif);
    _a = applyIF([x, y], IF), x = _a[0], y = _a[1];
  }

  return [x, y];
}

function generatePoint() {
  var x = Math.random() * 100;
  var y = Math.random() * 100;
  return [x, y];
}

function preparePoint(point, scale, canvasWidth, canvasHeight, translateX, translateY) {
  var x = Math.floor(point[0] * scale + translateX);
  var y = Math.floor((-point[1] + 1) * scale + translateY);
  return [x, y];
}

function asyncDrawPixel(context, point, color) {
  var x = point[0];
  var y = point[1];
  context.fillStyle = "rgba(" + color[0] + ", " + color[1] + ", " + color[2] + ", " + color[3] + ")";
  context.fillRect(x, y, 1, 1);
}

function createHistogram(canvasWidth, canvasHeight, histScale) {
  var histogram = [];

  for (var y = 0; y < canvasHeight * histScale; y++) {
    var row = [];

    for (var x = 0; x < canvasWidth * histScale; x++) {
      var histoCell = {
        color: null,
        frequency: 0
      };
      row.push(histoCell);
    }

    histogram.push(row);
  }

  return histogram;
}

function setHistogramColor(histogram, x, y, color) {
  if (!histogram[y][x].color) {
    histogram[y][x].color = color;
  } else {
    var newColor = histogram[y][x].color.map(function (channel, i) {
      return (channel + color[i]) / 2;
    });
    histogram[y][x].color = [newColor[0], newColor[1], newColor[2], newColor[3]];
  }

  histogram[y][x].frequency = histogram[y][x].frequency + 1;
  return histogram;
}

function getAverageColorMatrix(historgram, histScale) {
  var averageColorMatrix = [];

  for (var y = 0; y < historgram.length; y += histScale) {
    var row = [];

    for (var x = 0; x < historgram[y].length; x += histScale) {
      var colorSum = [0, 0, 0];

      for (var i = 0; i < histScale; i++) {
        for (var j = 0; j < histScale; j++) {
          var color = historgram[y + i][x + j].color;
          if (!color) continue;
          colorSum[0] += color[0];
          colorSum[1] += color[1];
          colorSum[2] += color[2];
        }
      }

      var averageColor = colorSum.map(function (color) {
        return color / histScale / histScale;
      });
      row.push(averageColor);
    }

    averageColorMatrix.push(row);
  }

  return averageColorMatrix;
}

function getAverageFreqMatrix(historgram, histScale) {
  var averageFreqMatrix = [];

  for (var y = 0; y < historgram.length; y += histScale) {
    var row = [];

    for (var x = 0; x < historgram[y].length; x += histScale) {
      var freqSum = 0;

      for (var i = 0; i < histScale; i++) {
        for (var j = 0; j < histScale; j++) {
          freqSum += historgram[y + i][x + j].frequency;
        }
      }

      row.push(freqSum / histScale / histScale);
    }

    averageFreqMatrix.push(row);
  }

  return averageFreqMatrix;
}

function getMaxFrequency(freqMatrix) {
  var maxFreq = -Infinity;
  freqMatrix.forEach(function (row, i) {
    row.forEach(function (freq) {
      if (freq > maxFreq) {
        maxFreq = freq;
      }
    });
  });
  return maxFreq;
}

function getAlphaMatrix(freqMatrix, maxFreq) {
  return freqMatrix.map(function (row) {
    return row.map(function (freq) {
      return Math.log10(freq / maxFreq);
    });
  });
}

function getPixelColorMatrix(averageColorMatrix, averageFreqMatrix, alphaMatrix, gamma) {
  return averageColorMatrix.map(function (row, i) {
    return row.map(function (color, j) {
      var alpha = alphaMatrix[i][j];
      var r = color[0] * Math.pow(alpha, 1 / gamma);
      var g = color[1] * Math.pow(alpha, 1 / gamma);
      var b = color[2] * Math.pow(alpha, 1 / gamma);
      var a = alpha * 255;
      return [r, g, b, a];
    });
  });
}

function drawPixelColorMatrix(pixelColorMatrix, canvas, canvasWidth, canvasHeight) {
  var ctx = canvas.getContext('2d');
  var imageData = ctx.getImageData(0, 0, canvasWidth, canvasHeight);
  var data = imageData.data;
  pixelColorMatrix.forEach(function (row, y) {
    row.forEach(function (color, x) {
      var index = (y * canvasWidth + x) * 4;
      data[index] = color[0];
      data[index + 1] = color[1];
      data[index + 2] = color[2];
      data[index + 3] = color[3];
    });
  });
  ctx.putImageData(imageData, 0, 0);
}
},{"./isifs":"isifs.ts","./utils":"utils.ts","./user_interface":"user_interface.ts"}],"C:/Users/vishn/AppData/Roaming/npm/node_modules/parcel/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "56484" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["C:/Users/vishn/AppData/Roaming/npm/node_modules/parcel/src/builtins/hmr-runtime.js","index.ts"], null)
//# sourceMappingURL=/courseWork.77de5100.js.map