function createMatrix(order: number): number[][] {
    const m = [];
    for(let i = 0; i < order; i++) {
        let str: Array<number> = new Array(order);
        str = str.map(num => 0);
        m.push(str);
    }
    return m;
}


function mult_matrix_vector(m: number[][], v: number[]) {
    const result = new Array(v.length);
    for(let i = 0; i < m.length; i++) {
        result[i] = 0;
        for(let j = 0; j < v.length; j++) {
            result[i] += v[j] * m[j][i];
        }
    }
    return result;
}

function MatrixVectorMultiplication (A, X)  {
    var A_l = A.length;
    for (var Y = new Array (A_l), j = 0; j < A_l; j++)
    {Y [j] = 0; for (var k = 0; k < A_l; k++) Y [j] += A [j] [k] * X [j]}
    return Y;   
}

function getRotateXMatrix(angle: number): number[][] {
    return [
        [1, 0, 0, 0],
        [0, Math.cos(angle), -Math.sin(angle), 0],
        [0, Math.sin(angle), Math.cos(angle), 0],
        [0, 0, 0, 1],
    ]
}

function getRotateYMatrix(angle: number): number[][] {
    return [
        [Math.cos(angle), 0, Math.sin(angle), 0],
        [0, 1, 0, 0],
        [-Math.sin(angle), 0, Math.cos(angle), 0],
        [0, 0, 0, 1],
    ]
}

function getRotateZMatrix(angle: number): number[][] {
    return [
        [Math.cos(angle), -Math.sin(angle), 0, 0],
        [Math.sin(angle), Math.cos(angle), 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1],
    ]
}

function translateDegToRad(deg: number): number {
    return Math.PI / 180 * deg;
}

function getRotateMatrix(axis: string, angle: number): number[][] {
    const radDeg = translateDegToRad(angle);
    switch(axis) {
        case 'x': return getRotateXMatrix(radDeg);
        case 'y': return getRotateYMatrix(radDeg);
        case 'z': return getRotateZMatrix(radDeg);
    }
}

function getTransfereMatrix(x: number = 0, y: number = 0, z: number = 0): number[][] {
    return [
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [x, y, z, 1]
    ]
}

function getStretchingMatrix(a: number = 1, b: number = 1, c: number = 1): number[][] {
    return [
        [a, 0, 0, 0],
        [0, b, 0, 0],
        [0, 0, c, 0],
        [0, 0, 0, 1],
    ]
}

function getMirrorImageMatrixXY(): number[][] {
    return [
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, -1, 0],
        [0, 0, 0, 1],
    ]
}

function getMirrorImageMatrixYZ(): number[][] {
    return [
        [-1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1],
    ]
}

function getMirrorImageMatrixXZ(): number[][] {
    return [
        [1, 0, 0, 0],
        [0, -1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1],
    ]
}

function getMirrorImageMatrix(axe: string) :number[][] {
    switch (axe) {
        case 'xy': return getMirrorImageMatrixXY();
        case 'yz': return getMirrorImageMatrixYZ();
        case 'xz': return getMirrorImageMatrixXZ();
    }
}

function getSkewMatrix() {
    return [
        [1, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1],
    ]
}

function getOrthographicProjectionMatrixX(p: number) : Array<[number, number, number, number]> {
    return [
        [0, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [p, 0, 0, 1]
    ]
}

function getOrthographicProjectionMatrixY(p: number) : Array<[number, number, number, number]> {
    return [
        [1, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 1, 0],
        [0, p, 0, 1]
    ]
}

function getOrthographicProjectionMatrixZ(p: number) :Array<[number, number, number, number]> {
    return [
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 0],
        [0, 0, p, 1]
    ]
}

function getAxonometricProjectioтMatrixZ(angleY: number, angleX: number) :number[][] {
    angleX = translateDegToRad(angleX);
    angleY = translateDegToRad(angleY);
    return [
        [Math.cos(angleY), Math.sin(angleX) * Math.sin(angleY), 0, 0],
        [0, Math.cos(angleY), 0, 0],
        [Math.sin(angleY), -Math.sin(angleY) * Math.cos(angleY), 0, 0],
        [0, 0, 0, 1]
    ]
}

function getOrthographicProjectionMatrix(axis: string = 'z', p: number): Array<[number, number, number, number]> {
    switch(axis) {
        case 'x': return getOrthographicProjectionMatrixX(p);
        case 'y': return getOrthographicProjectionMatrixY(p);
        default: return getOrthographicProjectionMatrixZ(p);
    }
}

function getCabinetProjectionMatrix(): number[][] {
    return getObliqueProjectionMatrix(0.5 * Math.cos(Math.PI / 4), 0.5 * Math.cos(Math.PI / 4));
}

function getObliqueProjectionMatrix(a, b) {
    return [
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [a, b, 0, 0],
        [0, 0, 0, 1],
    ]
}

function getFreeProjectionMatrix(): number[][] {
    return getObliqueProjectionMatrix(Math.cos(Math.PI / 4), Math.cos(Math.PI / 4));
}

function getCentralProjectionMatrix(a: number, b: number, c: number): number[][] {
    return [
        [1, 0, 0, -1/a],
        [0, 1, 0, -1/b],
        [0, 0, 1, -1/c],
        [0, 0, 0, 1],
    ]
}

function getSinglePointProjectionMatrix(r: number) {
    return [
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 1 / r],
        [0, 0, 0, 1],
    ]
}

const projectionMatrixes = {
    orthography: getOrthographicProjectionMatrix('z', 0),
    axonometric: getAxonometricProjectioтMatrixZ(10, 10),
    cabinet: getCabinetProjectionMatrix(),
    free: getFreeProjectionMatrix(),
    central: getCentralProjectionMatrix(1000, 1000, 1000),
    singlePoint: getSinglePointProjectionMatrix(800),
}

export {createMatrix, mult_matrix_vector, getRotateMatrix, projectionMatrixes, getTransfereMatrix, getStretchingMatrix, getMirrorImageMatrix};
