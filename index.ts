import {ISIF, SIFs} from "./isifs";
import {generateColor} from './utils';
import {generateButtons, generateControls} from './user_interface';

const canvas = <HTMLCanvasElement> document.getElementById("canvas");
const context = canvas.getContext('2d');

const CANVAS_WIDTH = 700;
const CANVAS_HEIGHT = 700;

canvas.setAttribute('width', String(CANVAS_WIDTH));
canvas.setAttribute('height', String(CANVAS_HEIGHT));

function getIF(sif: ISIF): number[] {
    const scale = 10000;
    const weight: number = Math.random() * scale;
    const IFLength = sif[0].length;
    let variationIndex = 0;
    let total = sif[variationIndex][IFLength - 1] * scale;
    while(weight > total) {
        variationIndex++;
        total += sif[variationIndex][IFLength - 1] * scale;
    }

    return sif[variationIndex];
}

function applyIF(point: [number, number], IF: number[]): number[] {
    const x = point[0];
    const y = point[1];

    const A = IF[0];
    const B = IF[1];
    const C = IF[2];
    const D = IF[3];
    const E = IF[4];
    const F = IF[5];

    const newX = A * x + B * y + E;
    const newY = C * x + D * y + F;

    return [newX, newY];

}

function main() {
    const coeffNotation = ['A', 'B', 'C', 'D', 'E', 'F', 'Probability'];
    const controlsContainer = <HTMLFormElement> document.getElementById('controls');
    const buttonsContainer = <HTMLFormElement> document.getElementById('sifs-buttons');
    const clearCanvasBtn = <HTMLButtonElement> document.getElementById('clearCanvas');
    clearCanvasBtn.addEventListener('click', () => {
        context.clearRect(0, 0, canvas.width, canvas.height);
    });
    const btnClickHandler = (controlsContainer, coeffNotation) => (e, key) => {
        const activeBtn = e.target.parentNode.querySelector('.btn.active');
        if(activeBtn) {
            activeBtn.classList.remove('active');
        }
        e.target.classList.add('active');
        generateControls(controlsContainer, SIFs[key], coeffNotation);
    };
    generateButtons(SIFs, buttonsContainer, btnClickHandler(controlsContainer, coeffNotation));
    // Adding listener to generate button
    const generateBtn = document.getElementById('generateButton');
    generateBtn.addEventListener('click', () => {
        const scaleInput = <HTMLInputElement> document.getElementById('scale-input');
        const translateXInput = <HTMLInputElement> document.getElementById('translate-x');
        const translateYInput = <HTMLInputElement> document.getElementById('translate-y');
        const iterationCountInput = <HTMLInputElement> document.getElementById('iteration-count');
        const dotCountInput = <HTMLInputElement> document.getElementById('dot-count');
        const scale = parseInt(scaleInput.value);
        const translateX: number = parseInt(translateXInput.value);
        const translateY = parseInt(translateYInput.value);
        const canvasWidth = canvas.clientWidth;
        const canvasHeight = canvas.clientHeight;
        const iterationCount = parseInt(iterationCountInput.value);
        const dotCount = parseInt(dotCountInput.value);


        asyncDrawFractal(canvas, getSIF(controlsContainer), {
            scale,
            iterationCount,
            dotCount,
            translateX,
            translateY,
            canvasWidth,
            canvasHeight,
            baseColor: [255, 0, 0, 255],
        });


       //draw(canvas, getSIF(controlsContainer));
    });
}

main()

function getSIF(controlsContainer: HTMLFormElement) {
    const inputs: NodeListOf<HTMLInputElement> = controlsContainer.querySelectorAll('.input-container__input');
    const table = [];
    let row = [];
    for(let i = 1; i <= inputs.length; i++) {
        row.push(parseFloat(inputs[i - 1].value));
        if(i % 7 === 0) {
            table.push(row);
            row = [];
        }
    }
    return table;
}

function asyncDrawFractal(canvas, sif: ISIF, params: {
    scale: number,
    dotCount: number,
    iterationCount: number,
    canvasWidth: number,
    canvasHeight: number,
    translateX: number,
    translateY: number,
    baseColor: [number, number, number, number]
}) {

    if(!sif || sif.length === 0) {
        alert('Ошибка! Введите коэффициенты!');
        return;
    }

    const {canvasWidth, canvasHeight, scale, dotCount, iterationCount, translateY, translateX, baseColor} = params;
    const histScale = 3;
    const gamma = 4;
    let histogram = createHistogram(canvasWidth, canvasHeight, histScale);

    console.log('Начали выполнение');

    for (let i= 0; i < dotCount; i++) {

        let [x, y] = generatePoint();
        [x, y] = calcNewPoint([x, y], sif, iterationCount);
        [x, y] = preparePoint([x, y], scale, canvasWidth, canvasHeight, translateX, translateY);
        if(x > 0 && x < canvas.width && y > 0 && y < canvas.height) {
            let randColor: [number, number, number, number] = generateColor();
            const color = randColor.map((channelNumber, index): number => (channelNumber + baseColor[index]) / 2 );
            histogram = setHistogramColor(histogram, x, y, [color[0], color[1], color[2], color[3]]);
            asyncDrawPixel(context, [x, y], [color[0], color[1], color[2], color[3]]);
        }
    }
    console.log("Закончили!");




    const averageColorMatrix = getAverageColorMatrix(histogram, histScale);
    const averageFreqMatrix = getAverageFreqMatrix(histogram, histScale);
    const maxFrequency = getMaxFrequency(averageFreqMatrix);
    const alphaMatrix = getAlphaMatrix(averageFreqMatrix, maxFrequency);
    const finalPixelColorMatrix = getPixelColorMatrix(averageColorMatrix, averageFreqMatrix, alphaMatrix, gamma)
    console.log(alphaMatrix);
    // drawPixelColorMatrix(finalPixelColorMatrix, canvas, canvasWidth, canvasHeight);

}


function calcNewPoint(point: [number, number], sif: ISIF, iterationCount: number): [number, number] {
    let x = point[0];
    let y = point[1];
    for(let i = 0; i < iterationCount; i++) {
        let IF = getIF(sif);
        [x, y] = applyIF([x, y], IF);
    }

    return [x, y];
}

function generatePoint(): [number, number] {
    let x = Math.random() * 100;
    let y = Math.random() * 100;
    return [x, y];
}

function preparePoint(point: [number, number], scale: number, canvasWidth: number, canvasHeight: number, translateX: number, translateY: number): [number, number] {
    const x = Math.floor(point[0] * scale + translateX);
    const y = Math.floor((-point[1] + 1) * scale + translateY);
    return [x, y];
}

function asyncDrawPixel(context, point: [number, number], color: [number, number, number, number]) {
    const x = point[0];
    const y = point[1];
    context.fillStyle = `rgba(${color[0]}, ${color[1]}, ${color[2]}, ${color[3]})`;
    context.fillRect(x,y,1,1);
}

function createHistogram(canvasWidth: number, canvasHeight: number, histScale: number): Array<Array<IHistoCell>> {
    const histogram: Array<Array<IHistoCell>> = [];
    for (let y = 0; y < canvasHeight * histScale; y++) {
        const row: Array<IHistoCell> = [];
        for (let x = 0; x < canvasWidth * histScale; x++) {
            const histoCell: IHistoCell = {
                color: null,
                frequency: 0
            }
            row.push(histoCell);
        }
        histogram.push(row);
    }
    return histogram;
}

function setHistogramColor(histogram: IHistoCell[][], x: number, y: number, color: [number, number, number, number]) {
    if(!histogram[y][x].color) {
        histogram[y][x].color = color;
    } else {
        const newColor  = histogram[y][x].color.map((channel, i) => (channel + color[i]) / 2)
        histogram[y][x].color = [newColor[0], newColor[1], newColor[2], newColor[3]];
    }
    histogram[y][x].frequency = histogram[y][x].frequency + 1;
    return histogram;
}

interface IHistoCell {
    color: [number, number, number, number] | null,
    frequency: number
}


function getAverageColorMatrix(historgram: IHistoCell[][], histScale: number): number[][][] {
    const averageColorMatrix: number[][][] = [];
    for(let y = 0; y < historgram.length; y += histScale) {
        const row = [];
        for(let x = 0; x < historgram[y].length; x += histScale) {
            let colorSum = [0, 0, 0];
            for(let i = 0; i < histScale; i++) {
                for(let j = 0; j < histScale; j++) {
                    const color = historgram[y + i][x + j].color;
                    if(!color) continue;
                    colorSum[0] += color[0];
                    colorSum[1] += color[1];
                    colorSum[2] += color[2];
                }
            }
            const averageColor = colorSum.map((color) => color / histScale / histScale);
            row.push(averageColor);
        }
        averageColorMatrix.push(row);
    }

    return averageColorMatrix;
}

function getAverageFreqMatrix(historgram: IHistoCell[][], histScale: number): number[][] {
    const averageFreqMatrix: number[][] = [];
    for(let y = 0; y < historgram.length; y += histScale) {
        const row = [];
        for(let x = 0; x < historgram[y].length; x += histScale) {
            let freqSum = 0;
            for(let i = 0; i < histScale; i++) {
                for(let j = 0; j < histScale; j++) {
                    freqSum += historgram[y + i][x + j].frequency;
                }
            }
            row.push(freqSum / histScale / histScale);
        }
        averageFreqMatrix.push(row);
    }

    return averageFreqMatrix;
}

function getMaxFrequency(freqMatrix: number[][]): number {
    let maxFreq = -Infinity;
    freqMatrix.forEach((row, i) => {
       row.forEach((freq) => {
           if(freq > maxFreq) {
               maxFreq = freq;
           }
       })
    });
    return maxFreq;
}


function getAlphaMatrix(freqMatrix: number[][], maxFreq: number) {
    return freqMatrix.map(row => {
       return row.map((freq) => Math.log10(freq / maxFreq));
    });
}

function getPixelColorMatrix(averageColorMatrix: number[][][],
                              averageFreqMatrix: number[][],
                              alphaMatrix: number[][],
                              gamma: number): number[][][] {
    return averageColorMatrix.map((row, i) => {
       return row.map((color, j) => {
           const alpha = alphaMatrix[i][j];
           const r = color[0] * Math.pow(alpha, 1 / gamma);
           const g = color[1] * Math.pow(alpha, 1 / gamma);
           const b = color[2] * Math.pow(alpha, 1 / gamma);
           const a = alpha * 255;
           return [r, g, b  , a];
       });
    });

}

function drawPixelColorMatrix(pixelColorMatrix: number[][][], canvas, canvasWidth, canvasHeight) {
    const ctx = canvas.getContext('2d');
    const imageData = ctx.getImageData(0, 0, canvasWidth, canvasHeight);
    const data = imageData.data;
    pixelColorMatrix.forEach((row, y) => {
        row.forEach((color, x) => {
            const index = (y * canvasWidth + x) * 4;
            data[index] = color[0];
            data[index + 1] = color[1];
            data[index + 2] = color[2];
            data[index + 3] = color[3];
        });
    });
    ctx.putImageData(imageData, 0, 0);
}